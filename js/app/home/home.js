// JavaScript Document
$("#content").html('')

$.ajaxSetup ({
   cache: false
});	

$("#content").load("html/profile/profile.html", function(){

var clientId;

function showData(){	

		$("#newPassword").val('')
		$("#oldPassword").val('')
		
		$("#name").val('')
		$("#phone").val('')
		$("#email").val('')
		$("#address").val('')
			
			
		$(".name").text('')
		$(".phone").text('')
		$(".email").text('')
		$(".address").text('')
			
		$(".username").text('')
		$(".password").text('')
			
		$("#point").text('')

	$.ajax({
	type     : 'GET',
    url      : 'http://27.131.253.12/smsgateway/demo/users/getProfile',
    dataType : 'JSON',
    cache: false,
    success  : function(data) { 
			
			if(data[0].level == 1){
				$(".user").hide()	
				$(".admin").show()
				
			}
			
			
			$("#greeting").text("Hi, "+data[0].name)
			
			$("#name").val(data[0].name)
			$("#phone").val(data[0].phone)
			$("#email").val(data[0].email)
			$("#address").val(data[0].address)
			
			$("#username").text(data[0].username)
			
			
			$(".name").text(data[0].name)
			$(".phone").text(data[0].phone)
			$(".email").text(data[0].email)
			$(".address").text(data[0].address)
			
			$(".username").text(data[0].username)
			$(".password").text(data[0].password)

            var point;

        if(data[0].balance != "NULL"){

            point = data[0].balance

        }else{

            point = 0

        }

			$("#point").text("Point : "+data[0].balance)
			
		
			
			if(data[0].img != ""){
				$(".imageProfile").attr("src", "img/profile/"+data[0].img);
				$(".imageProfile").attr("width", "150px");
			}
			
		}
	
		})
	}
	
	showData()
	

	//
	$("#saveProfile").submit(function(e)
	{
		
		var name = $("#name").val()
		var phone = $("#phone").val()
		var email = $("#email").val()
		var address = $("#address").val()
		
		var output = '';
		
		if ($.trim(name) == "") {
			output += "required NAME \n"
		}
		
		if ($.trim(phone) == "") {
			output += "required PHONE \n"
		}
		
		if ($.trim(email) == "") {
			output += "required EMAIL \n"
		}
		
		if ($.trim(address) == "") {
			output += "required ADDRESS \n"
		}
		
		if($.trim(name) == "" || $.trim(phone) == "" || $.trim(email) == "" || $.trim(address) == ""){
			alert(output)
		}else{
			output = ''
			
			var phoneNumber = '/[0-9-()+]{3,20}/'
		
			
			if(!/[0-9-()+]{3,20}$/.test($.trim(phone))){
				
				output += "phone required only number"
				alert(output)
				
			}else{
				
				 $("#profileStatus").text("saving... please wait...")
				
				 var formObj = $(this);
 				 var formURL = formObj.attr("action");
  				 var formData = new FormData(this);
  
    			$.ajax({
        			url: formURL,
    				type: 'POST',
        			data:  formData,
   	 				mimeType:"multipart/form-data",
    				contentType: false,
        			cache: false,
        			processData:false,
		
        			success:function(data) 
      				 {
							
							$("#profileStatus").text("")
							$('#inputModal').modal('hide')
							showData()
						
			
      				  }
   					 });
    				e.preventDefault()
    				e.unbind(); 
				 
			}
			
		}
		
	//	
    e.preventDefault()
    e.unbind(); 
		 		 
	});
	
	
	$("#savePassword").submit(function(e)
	{
	
	var newPassword = $("#newPassword").val()
	var oldPassword = $("#oldPassword").val()
	
	if ($.trim(newPassword) == "" || $.trim(oldPassword)  == "") {
		alert("Password cannot be empty")
	}else{
		
   	var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
		dataType : 'JSON',
        data : postData,
        success:function(data) 
        {
            if(data.message == "success"){
				$('#inputModal2').modal('hide')
				showData();
				$("#newPassword").val('')
				$("#oldPassword").val('')
			}else{
				alert(data.message)
				}
        	}
    	});
	}
		
	 e.preventDefault()
   	 e.unbind(); 
	
	})
	
	
	
   }
)
