// JavaScript Document
$("#content").html('')

$.ajaxSetup ({
   cache: false
});

$("#content").load("html/registration/registration.html", function(){

function getSchedule(){
	$.ajax({
	type     : 'GET',
    url      : 'http://localhost/rs/registration/getDoctorSchedule',
    dataType : 'JSON',
    cache: false,
    success  : function(data) {

	var schedule = '<ul class="list-group">'

	$.each(data, function(property, value) {

		 schedule += '<li class="list-group-item list-group-item-info">'+property+'</li>'

		 for(var i=0;i<value.length;i++){
			  schedule += '<li class="list-group-item"><input name="schedule" value='+value[i].id+' type=radio> '+value[i].t_start.substr(-8)+' - '+value[i].t_end.substr(-8)+' / '+value[i].dr_name+'</li>'
		 }

    });

	schedule += '</ul>'

	$('#listDokter').html(schedule)

	}

	})

}

function getIdentity(){
	$.ajax({
	type     : 'GET',
    url      : 'http://localhost/hospital/registration/getIdentity',
    dataType : 'JSON',
    cache: false,
    success  : function(data) {

		var listIdentity = '';

		for(var i=0;i<data.length;i++){
			listIdentity += '<option value='+data[i].identityId+'>'+data[i].description+'</option>'
		}

		$(".identity").html(listIdentity)

	}

	})

}

function insuranceEnableDisable(responsibly){

	if(responsibly == "self"){

		$('#responName').prop('disabled',true)
		$('#responIdentity').prop('disabled',true)
		$('#responNumberIdentity').prop('disabled',true)
		$('#responHp').prop('disabled',true)
		$('#responAddress').prop('disabled',true)

	}else if(responsibly == ""){

		$('#responName').prop('disabled',true)
		$('#responIdentity').prop('disabled',true)
		$('#responNumberIdentity').prop('disabled',true)
		$('#responHp').prop('disabled',true)
		$('#responAddress').prop('disabled',true)


	}else{

		$('#responName').prop('disabled',false)
		$('#responIdentity').prop('disabled',false)
		$('#responNumberIdentity').prop('disabled',false)
		$('#responHp').prop('disabled',false)
		$('#responAddress').prop('disabled',false)

	}
}

function getInsurance(){
	$.ajax({
	type     : 'GET',
    url      : 'http://localhost/rs/registration/getCustomer',
    dataType : 'JSON',
    cache: false,
    success  : function(data) {

		var listInsurance = '';

		listInsurance += '<option value="">--Pilih Jaminan--</option>'

		for(var i=0;i<data.length;i++){
			listInsurance += '<option value='+data[i].id+'>'+data[i].name+'</option>'
		}

		$("#insurance").html(listInsurance)
		$('#insurance').on('change', function(event) {
			if($(this).val()!=""){
				$('#responsibly').prop('disabled',false)
			}else{
				$('#responsibly').prop('disabled',true)
			}
		})

		$('#responsibly').on('change', function(event) {
			insuranceEnableDisable($(this).val())
		})
	}

	})

	}



	getIdentity()
	getInsurance()
	getSchedule()
	showData()

  $("#inputPasien").submit(function(e)
	{
		//alert('test')
   	var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
		dataType : 'JSON',
        data : postData,
        success:function(data)
        {
            if(data.message == "success"){
				$('#inputModal').modal('hide')
				showData();
			}
        }
    });

    e.preventDefault();
    e.unbind();

	});

  function showData(){

	$("#listSchedule").html('')

	$.ajax({
	  type     : 'GET',
    url      : 'http://localhost/rs/registration/getMedcaseQueue',
    dataType : 'JSON',
    cache: false,
    success  : function(data) {

		var list = ''

		 j=1
		 for(i=0;i<data.length;i++){

			var kehadiran;
			if(data[i].status == 0){ kehadiran='<button type="button check" class="btn btn-xs check" value="' +data[i].medcase_id+ '">Check</button>'}else{kehadiran='Hadir'}

		  list += '<tr><td>'+j+'</td><td>'+data[i].medcase_id+'</td><td></td><td>'+data[i].patient_name+'</td><td>'+data[i].queue+'</td><td>'+data[i].dr_name+'</td><td>'+data[i].customer_name+'</td><td>'+kehadiran+'</td></tr>'
		  j++
		 }

		$("#patientList").html(list)

    $('.check').click(function(){

      var medcase_id = $(this).attr("value");

      $.ajax({
          method : "GET",
             url : "http://localhost/rs/registration/check/" +medcase_id,
        dataType : 'JSON',
        success:function(data)
        {
          if(data.message == "success"){
            showData();
          }
        }
      });
    });

	}

	})

    }

})
