<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MDoctorSchedule extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }

  public function get(){

	$data = array();
	$department = array();

	$this->DBHospital->distinct();
	$this->DBHospital->select('dept_id,dept_name');
	$query = $this->DBHospital->get('view_dr_sched');

	foreach($query->result() as $row){

		$doctor = array();
		$query2 = $this->DBHospital->get_where('view_dr_sched', array('dept_id' => $row->dept_id));

		foreach($query2->result() as $row2){
			  $doctor[] = $row2;
		}

		$department[$row->dept_name] = $doctor;

	}

	return $department;

  }

  public function getDrIdById($id){
    $this->DBHospital->select('dept_id, dr_id');
  	$query = $this->DBHospital->get_where('view_dr_sched', ['id' => $id]);
    return array_shift($query->result_array());
  }

}

?>
