<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MMedcase extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }

  public function create($data) {
    $now = new DateTime();
    $data['create_time'] = $now->format('Y-m-d H:i:s');

    $this->DBHospital->insert('hos_medcase', $data);
  }

  public function getQueue(){

    $query = $this->DBHospital->get_where('view_patient_queue', array('dates' =>  date('Y-m-d')));

    return $query->result();

 }

}

?>
