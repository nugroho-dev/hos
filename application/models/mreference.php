<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MReference extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }

  public function create($data) {
    $now = new DateTime();
    $data['create_time'] = $now->format('Y-m-d H:i:s');

    $this->DBHospital->insert('hos_reference', $data);
    return $this->DBHospital->insert_id();
  }

  public function getByName($data){
    $this->DBHospital->select('name');
    $this->DBHospital->like('name', $data);
    $query = $this->DBHospital->get('hos_reference');

    return $query->result();
  }

}

?>
