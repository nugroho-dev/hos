<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MMedicalCase extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }

  public function create($data) {
    $now = new DateTime();
    $data['create_time'] = $now->format('Y-m-d H:i:s');

    $this->DBHospital->insert('hos_medcase', $data);

    $data2['medcase_id']  =  $this->DBHospital->insert_id();

    if($data['status'] == 1){
      $data2 = $this->getMaxQueueById($data2['medcase_id']);

      $data2['queue'] += 1;
    }

    $this->DBHospital->insert('hos_medcase_queue', $data2);
  }

  public function get(){

    //$this->DBHospital->like('t_start', date('Y-m-d'));
    $this->DBHospital->like('create_time', date('Y-m-d'));
    $query = $this->DBHospital->get('view_medcase_queue');

    return $query->result();
 }

 public function getMaxQueueById($id){
   $this->DBHospital->select('dr_sched_id');
   $query = $this->DBHospital->get_where('hos_medcase',['id' => $id,]);

   $result = array_shift($query->result_array());

   $this->DBHospital->select_max('queue');
   $query = $this->DBHospital->get_where('view_medcase_queue',['dr_sched_id' => $result['dr_sched_id']]);

   return array_shift($query->result_array());
 }

 public function updateQueueById($id){
   $data = $this->getMaxQueueById($id);
   $data['queue'] += 1;
   $this->DBHospital->where('medcase_id', $id);
   $this->DBHospital->update('hos_medcase_queue', $data);

   $data2['status'] = 1;
   $this->DBHospital->where('id', $id);
   $this->DBHospital->update('hos_medcase', $data2);
 }

}

?>
