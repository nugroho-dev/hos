<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MUsers extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }
  
  
  public function maxId(){
	  
	  	 $this->DBHospital->select_max('clientId');
		 $this->DBHospital->from("hos_user");
		 $query2 = $this->DBHospital->get();
	     foreach($query2->result() as $rows){
			return $rows->clientId ;
		 }
  }
  

   public function auth($data){
	   
	   $this->DBHospital->select("*");
  	   $this->DBHospital->from("sms_registration");
	   $this->DBHospital->where("authentication_code = '".$data['code']."' ");	
	   
	   $query = $this->DBHospital->get();
	   
	   if($query->num_rows() == 1)
	   {
		     foreach($query->result() as $row){
				
				$arrData = explode("~",$row->data);
				
				$dataRegist['name'] = $arrData[0];
				$dataRegist['phone'] = $arrData[1];
				$dataRegist['email'] = $arrData[2];
				$dataRegist['address'] = $arrData[3];
				$dataRegist['username'] = $row->username;
				$dataRegist['password'] = $row->password;
				
				$insert = $this->DBHospital->insert("hos_user", $dataRegist); 
				
				if($insert){
		   
		   		 $this->DBHospital->where('email', $dataRegist['email']);
				 $this->DBHospital->delete('sms_registration'); 
				 
				 $id = $this->maxId();
		   	
		  		 $this->DBHospital->select('clientId,username');
		    	 $this->DBHospital->from("hos_user");
				 $this->DBHospital->where("clientId = '".$id."'");
				 $query2 = $this->DBHospital->get();
				
					
				 foreach($query2->result() as $rows){
				
				 	$sess_array = array(	
	         		'clientId'=>$rows->clientId,
					'username'=>$rows->username
					);
				
					$this->session->set_userdata('logged_in', $sess_array);
		
				
					}
		   
		    	return true;
			
				}else{
					return false;	
				}
				
			}
		   	
	   }else{
			return false;   
	   }
	   
   }
  
   public function checkOldPassword($data){
	   
	   $this->DBHospital->select("password");
  	   $this->DBHospital->from("hos_user");
	   $this->DBHospital->where("clientId = '".$data['clientId']."' AND password = '".base64_encode($data['oldPassword'])."'");	
		
	   $query = $this->DBHospital->get();
	 
	   if($query->num_rows() == 1)
	   {
		    return true;
	   }else{
			return false;   
	   }
	   
   }
  
  
   public function editPassword($data){
	
	$check = $this->checkOldPassword($data);
	
	if($check){
		
		$dataPassword['password'] = base64_encode($data['newPassword']);
		$this->DBHospital->where('clientId', $data['clientId']);
		$this->DBHospital->update('hos_user', $dataPassword);
		
		return true;
		
	}else{
		 
		return false;
	}
 	
	
	  
  }
  
    public function editProfile($data){
	
	$this->DBHospital->where('clientId', $data['clientId']);
	$this->DBHospital->update('hos_user', $data);
 	
	return true; 
	  
  }
  
  
  public function getProfile($id){
	
    $this->DBHospital->select('*');
    $this->DBHospital->from('vw_profile');
	$where = "clientId = '".$id."'";
	$this->DBHospital->where($where);
    $query = $this->DBHospital->get();
	
    return $query->result();
	
	  
  }	
  
  
	function login($username,$password){
		
		$this->DBHospital->select("*");
  		$this->DBHospital->from("hos_user");
	    $this->DBHospital->where("username = '".$username."'");
	    $this->DBHospital->where("password = '".base64_encode($password)."'");
		$this->DBHospital->where("status = '1'");
	    $this->DBHospital->limit(1);
	 
	   $query = $this->DBHospital->get();
	 
	   if($query->num_rows() == 1)
	   {
		   foreach($query->result() as $row){
			   
				   $data['clientId'] = $row->clientId;
				   $data['username'] = $row->username;
				   $data['name'] = $row->name;
				
				   return $data;   
		   }
	   }
	   else
	   {
		    return false;
	   }
		
	}
	
		
  
  public function checkUser($data){
		
		$this->DBHospital->select("username,email");
  		$this->DBHospital->from("sms_registration");
	    $this->DBHospital->where("email = '".$data['email']."' OR username = '".$data['username']."'");	
		
	   $query = $this->DBHospital->get();
	 
	   if($query->num_rows() == 1)
	   {
		    return false;
	   }else{
			return true;   
	   }
			
			  
  }
  
  
   public function checkProfile($data){
		
		$this->DBHospital->select("username,email");
  		$this->DBHospital->from("hos_user");
	    $this->DBHospital->where("username = '".$data['username']."' OR email='".$data['email']."'");	
		
	   $query = $this->DBHospital->get();
	 
	   if($query->num_rows() == 1)
	   {	
		    return false;
			
	   }else{
			return true;   
	   }
			
			  
  }
  
  public function generateRandomString($length = 20) {
	  
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
	
	}
	
  
  public function registration($data){
	  
	   if($this->checkUser($data) && $this->checkProfile($data)){
		
		$randString = $this->generateRandomString();
		   
		$dataRegist['email'] =  $data['email'];
		$dataRegist['authentication_code'] =  $randString;
		$dataRegist['username'] =  $data['username'];
		$dataRegist['password'] =  base64_encode($data['password']);
		$dataRegist['data'] =  $data['name']."~".$data['phone']."~".$data['email']."~".$data['address'];
		   
		$insert = $this->DBHospital->insert("sms_registration", $dataRegist);
		
		return $randString;
		  	   
	   }else{
		  return false;  
	   }
	  
	  
  }
  
  
 /*  public function registration($data){
	 
	 
	 if($this->checkUser($data)){
	 
	 	$insert = $this->DBHospital->insert("hos_user", $data); 
		
		if($insert){
			
			$this->DBHospital->select('clientId');
    		$this->DBHospital->from('hos_user');
			
			$where = "username = '".$data['username']."' AND password = '".$data['password']."'";
			$this->DBHospital->where($where);	
    		$query = $this->DBHospital->get();
	
    		foreach($query->result() as $row){
			
				$clientId = $row->clientId;
				
			
			}
			
			$sess_array = array(	
	         		'clientId'=>$clientId,
					'username'=>$data['username']
	       	);
		
					
			$this->session->set_userdata('logged_in', $sess_array);
			
		}
		
		if($this->session->userdata('logged_in')){
	  		return true;
	  	 }
		 
	 }else{
		return false; 
	 }
	 
 }*/
 
  
  
  
  
}

?>