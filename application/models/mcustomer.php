<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MCustomer extends CI_Model{

  public function __construct (){
     parent::__construct();
	 $this->DBHospital = $this->load->database("hospital",true);
  }

  public function get(){

    $query = $this->DBHospital->get('hos_customer');

    return $query->result();
 }

}

?>
