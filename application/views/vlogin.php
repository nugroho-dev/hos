<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Login Sms Front End</title>
<link href="../css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="../css/bootstrap/sb-admin-2.css" rel="stylesheet">
<link href="../css/bootstrap/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4">
<div class="login-panel panel panel-default">
<div class="panel-heading"><h3 class="panel-title" align="center">SMS FRONT END</h3></div>
<div class="panel-heading"><h3 class="panel-title">Sign In</h3></div>
<div class="panel-body">
<form action="verifylogin" role="form" method="post">
<fieldset>
<div class="form-group"><input class="form-control" placeholder="Username" name="username" type="username" autofocus></div>
<div class="form-group"><input class="form-control" placeholder="Password" name="password" type="password"></div>
<div class="checkbox"><label><input name="remember" type="checkbox" value="Remember Me">Remember Me</label></div>
<input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
<a href="register" class="btn btn-lg btn-success btn-block">Register</a>
</fieldset>
</form><br>
<div id="validation" align="center" style="color:#F00"> <?php echo validation_errors(); ?></div>
</div></div></div></div></div>
</body>
</html>
