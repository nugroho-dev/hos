<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>RJ HOSPITAL</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<link href="css/bootstrap/sb-admin-2.css" rel="stylesheet">
<link href="css/bootstrap/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<script src="js/bootstrap/jquery-1.11.0.js"></script>

<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/bootstrap/plugins/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/bootstrap/sb-admin-2.js"></script>

<!-- load home -->
<script src="js/app/home/navigation.js"></script>


</head>

<body>


<!-- Navigation -->

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.html"><img  height="30" src="img/logo.png" style="margin-top:-10px" />&nbsp;&nbsp;RJ Hospital Application</a>
</div>
<div id="greeting" align="right" style=" width:100%;margin-top:20px;margin-left:-20px; position:fixed;">&nbsp;</div>
<ul class="nav navbar-top-links navbar-right"></ul>
<div class="navbar-default sidebar" role="navigation">

<div id="user" class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">


<li class="user"><a href="#"> REGISTRASI <span class="fa arrow"></span></a>
<ul class="nav nav-second-level">
<li><a href="#" id="">Data Pasien</a></li>
<li><a href="#" id="">Rawat Jalan</a></li>
<li><a href="#" id="">Rawat Inap</a></li>
<li><a href="#" id="">Medical Checkup</a></li>

</ul>
</li>

<li class="user"><a href="#">KONSUL DOKTER<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Input Status Pasien</a></li>
<li><a href="#" id="">Billing Pasien</a></li>
<li><a href="#" id="">Order Diagnosa</a></li>
<li><a href="#" id="">Pemeriksaan Medis</a></li>
<li><a href="#" id="">Resep Dokter</a></li>
<li><a href="#" id="">Report</a></li>

</ul>
</li>

<li class="user"><a href="#">KASIR<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Rawat Jalan</a></li>
<li><a href="#" id="">Rawat Inap</a></li>
<li><a href="#" id="">Medical CheckUp</a></li>
<li><a href="#" id="">Report</a></li>

</ul>
</li>

<li class="user"><a href="#">REKAM MEDIS<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Info Status Pasien - R.Jalan</a></li>
<li><a href="#" id="">Info Status Pasien - R.Inap</a></li>
<li><a href="#" id="">Report</a></li>

</ul>
</li>

<li class="user"><a href="#">NURSE STATION<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Order Transaksi</a></li>
<li><a href="#" id="">Diet Pasien</a></li>
<li><a href="#" id="">Diagnosa Pasien</a></li>
<li><a href="#" id="">Ruang Perawatan Kosong</a></li>
<li><a href="#" id="">Report</a></li>

</ul>
</li>

<li class="user"><a href="#">FARMASI<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Pengecekan Obat Oleh AA</a></li>
<li><a href="#" id="">Antrian Obat</a></li>
<li><a href="#" id="">Pesanan Barang</a></li>
<li><a href="#" id="">Terima Barang</a></li>
<li><a href="#" id="">Stock OpName</a></li>
<li><a href="#" id="">Report</a></li>

</ul>
</li>

<li class="user"><a href="#">BACKOFFICE<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Cek List Transaksi Pasien</a></li>
<li><a href="#" id="">Pembayaran Gaji Dokter</a></li>
<li><a href="#" id="">Tagihan Perusahaan</a></li>
<li><a href="#" id="">Schedule Tagihan</a></li>
<li><a href="#" id="">Pelunasan Perusahaan</a></li>
<li><a href="#" id="">Hutang Perusahaan</a></li>
<li><a href="#" id="">Pelunasan Hutang</a></li>

</ul>
</li>

<li class="user"><a href="#">REPORT<span class="fa arrow"></span></a>
<ul class="nav nav-second-level">

<li><a href="#" id="">Kumpulan Report Generator</a></li>

</ul>
</li>

<li >
<a href="users/logout">Logout</a>
</li>

</ul>

</div>
</div></nav><div id="page-wrapper">
<div class="row">
<div id="content" style=" margin-left:20px; margin-right:20px;margin-top:5px;"></div>
</div>

</div>

</body>

</html>
