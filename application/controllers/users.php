<?php

class Users extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->load->library('session');
		$this->load->model('musers');
	}
	


	function auth(){
		
		$data['code'] = (isset($_POST['code']) ? $_POST['code'] : "");	
		$data['code'] = trim($data['code']);
		$auth =  $this->musers->auth($data);
		if($auth){
			echo '{"message":"success"}';
		}else{
			echo '{"message":"failed"}';
		}
		
	}
	
	function editPassword(){
	  
	  if(!$this->session->userdata('logged_in')){
	  		redirect('users/login', 'refresh');
	   }
	   
	  $dataLogin = $this->session->userdata('logged_in');
	  
	  $data['clientId'] = $dataLogin['clientId']; 
	  $data['oldPassword'] = (isset($_POST['oldPassword']) ? $_POST['oldPassword'] : "");
	  $data['newPassword'] = (isset($_POST['newPassword']) ? $_POST['newPassword'] : "");
	  $data['newPassword'] =  $data['newPassword'];
	  $edit =  $this->musers->editPassword($data);
	  
	  if($edit){
		  	echo '{"message":"success"}'; 
	   }else{
		   echo '{"message":"error old password"}';
	  }
	  
	  	
	}
	
	function editProfile()
	{ 
	
	   if(!$this->session->userdata('logged_in')){
	  		redirect('users/login', 'refresh');
	   }
	   
	  $dataLogin = $this->session->userdata('logged_in');
	  $data['clientId'] = $dataLogin['clientId']; 
	 
	  $data['name'] = (isset($_POST['name']) ? $_POST['name'] : "");
	  $data['phone'] = (isset($_POST['phone']) ? $_POST['phone'] : "");
	  $data['email'] = (isset($_POST['email']) ? $_POST['email'] : "");
	  $data['address'] = (isset($_POST['address']) ? $_POST['address'] : "");
	  $data['img'] = (isset($_FILES['img']['name']) ? $_FILES['img']['name'] : "");
	  
	  if(!empty($data['img'])){
	  	move_uploaded_file($_FILES["img"]["tmp_name"],"img/profile/" . $_FILES["img"]["name"]);
	  }
	   
	  $edit =  $this->musers->editProfile($data);
	  
	  if($edit){
		  	echo '{"message":"success"}'; 
	   }else{
		   echo '{"message":"failed"}';
	   }
	  
		
	 
	
	}

	function getProfile()
	{ 
		if(!$this->session->userdata('logged_in')){
	  		redirect('users/login', 'refresh');
	   }
		
	  $dataLogin = $this->session->userdata('logged_in');
	  $id = $dataLogin['clientId'];
	  $dataUser =  $this->musers->getProfile($id);
	  $frontChar = substr(base64_decode($dataUser[0]->password),0,2);
	  $totalStr = strlen(base64_decode($dataUser[0]->password));
	  $hidden = str_repeat('*', $totalStr-2);
	  $dataUser[0]->password = $frontChar.$hidden;
	  echo json_encode($dataUser); 
	  
	}
	
	function sendEmail($code,$email){
	
	  	error_reporting(E_STRICT);
		date_default_timezone_set("Asia/Jakarta");
		require_once(FCPATH .'mailer/class.phpmailer.php');
		
		$mail             = new PHPMailer();
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->IsHTML(true);
		$mail->Host       = "mail.foreverapps.co.id"; // SMTP server
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages                                   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		
		$mail->Host       = "mail.foreverapps.co.id"; // sets the SMTP server
		$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
		$mail->Username   = "foreverapps@foreverapps.co.id"; // SMTP account username
		$mail->Password   = "maduroo123";        // SMTP account password

		$mail->SetFrom('foreverapps@foreverapps.co.id', 'ForeverApps');

		$mail->AddReplyTo("foreverapps@foreverapps.co.id","ForeverApps");

		$mail->Subject    = "Sms Blasting Authentication";

		$mail->AltBody    = "Authentication"; // optional, comment out and test
		$mail->Body = "for authentication copy and paste this code <br> CODE : ".$code."";

		$address = $email;
		$mail->AddAddress($address, "Authentication");


		if(!$mail->Send()) {
  			return false;
		} else {
 			return true;
		}
		 	
	}
	
	
	function register()
	{ 
	
	
	   if(isset($_POST['regis'])) {
		   
	   $data['username'] = (isset($_POST['username']) ? $_POST['username'] : "");
	   $data['password'] = (isset($_POST['password']) ? $_POST['password'] : "");
	   $data['name'] = (isset($_POST['name']) ? $_POST['name'] : "");
	   $data['phone'] = (isset($_POST['phone']) ?  $_POST['phone'] : "");
	   $data['email'] = (isset($_POST['email']) ? $_POST['email'] : "");
	   $data['address'] = (isset($_POST['address']) ? $_POST['address'] : "");
	   
	   $regis =  $this->musers->registration($data);
	   
	   if($regis){
			// redirect('home', 'refresh');  
			
			$this->sendEmail($regis,$data['email']);	
		
			echo '{"message":"success"}'; 
	   }else{
		   echo '{"message":"Username or email has been taken"}';
	   }
	  
	   }else{
		   
		   $this->load->view('vregis');
		   
	   }
	  
	}
	
	
	
	function success(){
		$this->load->view('vsuccess');	
	}
	
	function login()
	{ 

	   $this->load->helper(array('form', 'url'));
	   $this->load->view('vlogin');
	}
	
	
	
	function verifylogin(){
		
		
 		 $this->load->library('form_validation');
	  	 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
  		 $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
	 
   		if($this->form_validation->run() == FALSE)
	  	 {
    		$this->load->view('vlogin');
	   	 }
	    else
	   	{
    	
	     //echo var_dump($this->session->all_userdata());
     	redirect('home', 'refresh');
		
	   	}
		
	}
	
	function check_database($password)
	 {
		 
	  
  		$username = $this->input->post('username');
		$password= $this->input->post('password');
		
	    $result = $this->musers->login($username, $password);
	 
	   if($result)
   		{
			
       		$sess_array = array(
					
	         		'clientId'=>$result['clientId'],
					'username'=>$result['username'],
					'name'=>$result['name'],
					'bulkId'=>''
					
	       	);
		
					
			$this->session->set_userdata('logged_in', $sess_array);
					
	    return TRUE;
	   }
	   else
	   {
	     $this->form_validation->set_message('check_database', 'Invalid username or password');
	     return false;
  		 }
		 
	}
	
	
	function logout()
	{
		session_start(); 
		$this->session->unset_userdata('session_id');
		session_destroy();
		redirect('users/login', 'refresh');	
			
	}
	
	
	
}