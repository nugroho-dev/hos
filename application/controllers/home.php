<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	
		function __construct(){
		
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
	  		 	redirect('users/login', 'refresh');
	   		}
	
	    }
	
	 	function index()
		{
			 $this->load->view('index');
		}
	
}

?>