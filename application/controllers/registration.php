<?php

class Registration extends CI_Controller{

	/*member variable*/

	/*dynamic variable*/

	function __construct(){
		parent::__construct();

		$this->load->library('session');
		$this->load->model([
				'mpatient', 'mreference', 'mcustomer',
				'mmedicalcase', 'mdoctorschedule'
			]);
	}

	//pasien rawat jalan
	/*************************/

	function getMedcaseQueue(){
		echo json_encode($this->mmedicalcase->get());
	}

	function getDoctorSchedule(){
		echo json_encode($this->mdoctorschedule->get());
	}

	function getCustomer(){
		echo json_encode($this->mcustomer->get());
	}

	function getReference($data){
		echo json_encode($this->mreference->getByName($data));
	}

	function store(){

		$patient = [
				'name'			=> $_POST['name'],
				'alias' 		=> $_POST['alias'],
				'birthdate' => $_POST['birthdate'],
				'phone' 		=> $_POST['phone'],
				'address'		=> $_POST['address'],
				'gender'		=> $_POST['gender'],
			];

		$patient_id = $this->mpatient->create($patient);

		$reference['name'] = $_POST['reference'];

		$reference_id = $this->mreference->create($reference);

		$schedule = $this->mdoctorschedule->getDrIdById($_POST['schedule']);

		$medicalcase = [
				'patient_id'					=> $patient_id,
				'dr_sched_id'					=> $_POST['schedule'],
				'dr_id'								=> $schedule['dr_id'],
				'dept_id'							=> $schedule['dept_id'],
				'reference_id'				=> $reference_id,
				'care_type_id'				=> '',
				'customer_id'					=> $_POST['customer_id'],
				'customer_family' 		=> $_POST['customer_family'],
				'customer_name'				=> $_POST['customer_name'],
				'customer_nik'				=> $_POST['customer_nik'],
				'customer_occupation'	=> $_POST['customer_occupation'],
				'status'							=> $_POST['appointment'],
				'arrive_time'					=> date("Y-m-d H:i:s")
			];

		$this->mmedicalcase->create($medicalcase);

		echo '{"message":"success"}';

	}

	function check($id)
  {

    $this->mmedicalcase->updateQueueById($id);

    echo '{"message":"success"}';

  }

}
