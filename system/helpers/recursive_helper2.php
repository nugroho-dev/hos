<?php

$getTotal = 0;


function save_recursive($type,$from,$id,$thn,$parent=0){
	
	//check parent dan type
	$ci =& get_instance(); 
	$ci->load->database();
	
	if($from == "apbd_provinsi_rekening"){
		$wilRekening='idProvinsiRekening';
	}else{
		$wilRekening='idKabupatenRekening';
	}


	$where = "parent = '".$parent."' AND type = '".$type."' AND (".$wilRekening." = '0' OR ".$wilRekening." = '".$id."')";

	$ci->db->select('*');
	$ci->db->from($from);
	$ci->db->where($where);

	$queryListRekening = $ci->db->get();
	$resultListRekening = $queryListRekening->result();
	//
	foreach ($resultListRekening as $rowListRekening)
	{
		//check data pada table jumlah 
		if($from == "apbd_provinsi_rekening"){
		     
			 $provinsi = true;
			 $perda = '';
			 $fromJumlah = "apbd_provinsi_jumlah";
	
			 $where = "idProvinsi = '".$id."' AND idProvRekening = '".$rowListRekening->idProvRekening."'";	
			 $ci->db->select('*');
		
		}else{
		
			 $provinsi = false;
			 $perda = '';
			 $fromJumlah = "apbd_kabupaten_jumlah";
			
			 $where = "idKabupaten = '".$id."' AND idKabRekening = '".$rowListRekening->idKabRekening."'";	
			 $ci->db->select('*');
	
		}
			
		$ci->db->from($fromJumlah);
		$ci->db->where($where);
   	
   		
		$queryCheckRekening = $ci->db->get();
		$queryCheckRekeningResult = $queryCheckRekening->result();
		$countCheckRekening = $queryCheckRekening->num_rows(); 
	
		if($countCheckRekening != 0){
				foreach ($queryCheckRekeningResult as $CheckRekeningResult)
			{
				if(isset($_POST['simpansementara'])){
						$perda = '';
						$idWilayah = '';
						$idWilayahRekening = '';
						$value = '';
						$vwfrom =  '';
						if($provinsi){
							
							$vwfrom = "vw_apbd_provinsi_total";
							
							$perda = $_POST['perda'.$rowListRekening->idProvRekening];
							
							if(isset($_POST['prov'.$rowListRekening->idProvRekening])){
							
							//echo $_POST['prov'.$rowListRekening->idProvRekening];
							$value = $_POST['prov'.$rowListRekening->idProvRekening];
							$where =  "idProvinsi = '".$id."' AND idProvRekening = '".$rowListRekening->idProvRekening."'";
							
							$idWilayahRekening = $rowListRekening->idProvRekening;
							$idWilayah = $id;
							
							
						
							
							}
							
						}else{
							
							$vwfrom = "vw_apbd_kabupaten_total";
							
							$perda = $_POST['perda'.$rowListRekening->idKabRekening];	
							
							if(isset($_POST['kab'.$rowListRekening->idKabRekening])){
							
						
							$value = $_POST['kab'.$rowListRekening->idKabRekening];
							$where =  "idKabupaten = '".$id."' AND idKabRekening = '".$rowListRekening->idKabRekening."'";
						
							$idWilayahRekening = $rowListRekening->idKabRekening;
							$idWilayah = $id;
							
						
							}
						}
						
						$data = array(
							'perda' => $perda,
               				'jumlah' => $value,
              			 	'date' => time()
            				);

						$ci->db->where($where);
						$update = $ci->db->update($fromJumlah, $data); 
						
						if($update){
							updateSaveParent($vwfrom,$idWilayah,$idWilayahRekening,$value);
						}
						
					}
				
			
			}
			
			
			
		}
		
		if($provinsi){
    		save_recursive($type,$from,$id,$thn,$rowListRekening->idProvRekening);
		}else{
			save_recursive($type,$from,$id,$thn,$rowListRekening->idKabRekening);
		}
	}
		
	
}





function display_recursive($type,$from,$id,$thn,$viewType,$parent=0,$level=1){ 
	
	//echo $viewType;
	//check parent dan type
	$ci =& get_instance(); 
	$ci->load->database();
	
	if($from == "apbd_provinsi_rekening"){
		$wilRekening='idProvinsiRekening';
	}else{
		$wilRekening='idKabupatenRekening';
	}

	$where = "parent = '".$parent."' AND type = '".$type."' AND (".$wilRekening." = '0' OR ".$wilRekening." = '".$id."')";

	$ci->db->select('*');
	$ci->db->from($from);
	$ci->db->where($where);

	$queryListRekening = $ci->db->get();
	$resultListRekening = $queryListRekening->result();
	//

	$html= "";
	$jumlah = 0;
	$j = 0;
	
	

	foreach ($resultListRekening as $rowListRekening)
	{

   		for($i=1;$i<=$level;$i++){
   			$html .= $i."-";
   		}
		
		
		//check data pada table jumlah 
		if($from == "apbd_provinsi_rekening"){
		     
			 $provinsi = true;
			 $perda = '';
			 
			 $wilFinal='idProvinsi';
			 $fromFinal = 'vw_provinsi_final';
			 $whereFinal = "idProvinsi = '".$id."'";
			 
			 $fromJumlah = "apbd_provinsi_jumlah";
				
			 $where = "idProvinsi = '".$id."' AND idProvRekening = '".$rowListRekening->idProvRekening."'";	
			 $ci->db->select('*');
			 
			 $data = array(
               			'idProvinsi' => $id,
               			'idProvRekening' => $rowListRekening->idProvRekening,
						'perda' => '',
               			'jumlah' => '',
						'date' => ''
            );
		
		}else{
			
			
			 $provinsi = false;
			 $perda = '';
			 
			 $wilFinal='idKabupaten';
			 $fromFinal = 'vw_kabupaten_final';
			 $whereFinal = "idKabupaten = '".$id."'";
			 
			 
			 $fromJumlah = "apbd_kabupaten_jumlah";
			
			 $where = "idKabupaten = '".$id."' AND idKabRekening = '".$rowListRekening->idKabRekening."' ";
			 $ci->db->select('*');
			 
			 $data = array(
               			'idKabupaten' => $id,
               			'idKabRekening' => $rowListRekening->idKabRekening,
						'perda' => '',
               			'jumlah' => '',
						'date' => ''
            );
	
		}
			
		$ci->db->from($fromJumlah);
		$ci->db->where($where);
   			
 		$html = substr_replace($html,'',-1);
   		
		$queryCheckRekening = $ci->db->get();
		$queryCheckRekeningResult = $queryCheckRekening->result();
		$countCheckRekening = $queryCheckRekening->num_rows(); 
		
		if($viewType == "view"){
				
				$ci->db->select($wilFinal);
				$ci->db->from($fromFinal);
				$ci->db->where($whereFinal);
				
				$queryFinal = $ci->db->get();
				$queryCheckFinal = $queryFinal->result();
				$countCheckFinal = $queryFinal->num_rows(); 
				
				if($countCheckFinal == 0){
				
				echo "<tr><td style='color:#F00;'>DATA BELUM TERSIMPAN PADA PENYIMPANAN AKHIR</td></tr>";
				exit;
				
				}
		    }
		
		//jika ada, maka masukan data
		if($countCheckRekening == 0){
			
			$value = 0;
			$ci->db->insert($fromJumlah,$data);	
			
		}else{
			foreach($queryCheckRekeningResult as $rekeningResult){	
				$value = $rekeningResult->jumlah;
				$perda = $rekeningResult->perda;
			}
			
			
		}
		//
		
	
  	 	echo "
    		<tr id=".$html." class=a>
    		<td id=p1><div id=p2 class=tier".$level."><a id=p3 class=folder></a><table border=0><tr><td> </td><td>".$rowListRekening->deskripsi."</td></tr></table></div></td>
   			 <td>".$rowListRekening->kodeRekening."</td>";
			 
			
		 if($viewType == "input"){
			if($provinsi){
    		echo "<td> <input name=perda".$rowListRekening->idProvRekening." style='width:100%' type=text value = '".$perda."'  />";
			}else{
			echo "<td> <input name=perda".$rowListRekening->idKabRekening." style='width:100%' type=text value = '".$perda."' />";	
			}
		 }else{
			 echo "<td>".$perda."</td>";	
		 }
		
			
			echo "</td>";
			
			if($from == "apbd_provinsi_rekening"){
				
			 $where = "parent = '".$rowListRekening->idProvRekening."'";	
			 $ci->db->select('idProvRekening');
		
			}else{
				
		
			 $where = "parent = '".$rowListRekening->idKabRekening."'";	
			 $ci->db->select('idKabRekening');
	
			}
			
			$ci->db->from($from);
			$ci->db->where($where);
   			
 			$html = substr_replace($html,'',-1);
   		
			$queryCheckId = $ci->db->get();
			$countCheckId = $queryCheckId->num_rows(); 
			
			//check child terdalam
			if($countCheckId == 0){
			 if($viewType == "input"){
				if($provinsi){			
					echo "<td><span style='float:left'>Rp.&nbsp;&nbsp;&nbsp;</span><input style='text-align:right' name=prov".$rowListRekening->idProvRekening." type=text value=".$value." /></span></td>";	
				}else{
					
					echo "<td><span style='float:left'>Rp.&nbsp;&nbsp;&nbsp;</span><input style='text-align:right' name=kab".$rowListRekening->idKabRekening." type=text value=".$value." /></span></td>";	
				}
			 }else{
				 
				echo "<td align='right' style='text-align:right'><b><span style='float:left'>Rp. </span>".number_format( $value , 0 , '' , '.' )." </b></td>";	 
				
			 }
				
			}else{
	
			echo "<td align='right' style='text-align:right'><b><span style='float:left'>Rp. </span>".number_format( $value , 0 , '' , '.' )."</b> </td>";	
		
			
	   if($viewType == "input"){
			
			if(strlen($rowListRekening->kodeRekening) == 5 || strlen($rowListRekening->kodeRekening) == 7 ||  strlen($rowListRekening->kodeRekening) == 8){
					$idRekening = '';
					if($provinsi){			
							$idRekening = $rowListRekening->idProvRekening;
						}else{
							$idRekening = $rowListRekening->idKabRekening;	
						}
					
					echo "<td><a data-id='".$idRekening."' data-idParent='".$rowListRekening->parent."' data-deskripsi='".rawurlencode($rowListRekening->deskripsi)."' data-noRek='".$rowListRekening->kodeRekening."' data-jumlah='".$value."' href='' class='modalInput' rel='#prompt'>edit</td>";
			}
				
			}
			
			
			}
			
		
			
   			echo "</tr>";
  
	if($provinsi){
    	display_recursive($type,$from,$id,$thn,$viewType,$rowListRekening->idProvRekening,$level+1);
	}else{
		display_recursive($type,$from,$id,$thn,$viewType,$rowListRekening->idKabRekening,$level+1);
	}
   $html= "";
   
   
	}
	
} 


function updateSaveParent($vwfrom,$idWilayah,$idWilayahRekening,$value){

	$ci =& get_instance(); 
	$ci->load->database();
	
	if($vwfrom == "vw_apbd_provinsi_total"){
		$fromJumlah = "apbd_provinsi_jumlah";
		$fieldWilayah = "idProvinsi";
		$fieldWilayahRekening = "idProvRekening";	
	}else{
		$fromJumlah = "apbd_kabupaten_jumlah";
		$fieldWilayah = "idKabupaten";
		$fieldWilayahRekening = "idKabRekening";
	}

	$where = $fieldWilayahRekening." = '".$idWilayahRekening."' AND ".$fieldWilayah." = '".$idWilayah."'";
	$ci->db->select('parent');
	$ci->db->from($vwfrom);
	$ci->db->where($where);
		
	$query = $ci->db->get();
	$result =  $query->result();
	
	
	
	foreach ($result as $row)
	{
		
		//echo $row->parent;
		
		$where = "parent = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
		$ci->db->select('SUM(jumlah) AS jumlah',FALSE);
		$ci->db->from($vwfrom);
		$ci->db->where($where);
		
		$queryParent = $ci->db->get();
		$resultParent =  $queryParent->result();
		
		foreach ($resultParent as $rowParent)
		{
			//$rowParent->jumlah;
			
			$data = array(
            'jumlah' => $rowParent->jumlah,
            'date' => time()
         );
		
		$where = $fieldWilayahRekening." = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
		$ci->db->where($where);
		$update = $ci->db->update($fromJumlah, $data);
		
		
		if($update){
		
			//$where = $fieldWilayahRekening." = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
			
				$ci->db->select($fieldWilayahRekening.",jumlah");
				$ci->db->from($vwfrom);
				$ci->db->where($where);
			
		
			$queryParent2 = $ci->db->get();
			$resultParent2 =  $queryParent2->result();
			
			foreach ($resultParent2 as $rowParent2){
			
			if($vwfrom == "vw_apbd_provinsi_total"){
				$id = $rowParent2->idProvRekening;
			}else{
				$id = $rowParent2->idKabRekening;
			}
		
			updateParent($vwfrom,$idWilayah,$id,$rowParent2->jumlah);
				
			}
			
			
			
			
		}
		
		
		}
		
		
		
			
				
	}

	
}






function updateParent($vwfrom,$idWilayah,$idWilayahRekening){

	$ci =& get_instance(); 
	$ci->load->database();
	
	if($vwfrom == "vw_apbd_provinsi_total"){
		$fromJumlah = "apbd_provinsi_jumlah";
		$fieldWilayah = "idProvinsi";
		$fieldWilayahRekening = "idProvRekening";	
	}else{
		$fromJumlah = "apbd_kabupaten_jumlah";
		$fieldWilayah = "idKabupaten";
		$fieldWilayahRekening = "idKabRekening";
	}

	$where = $fieldWilayahRekening." = '".$idWilayahRekening."' AND ".$fieldWilayah." = '".$idWilayah."'";
	$ci->db->select('parent');
	$ci->db->from($vwfrom);
	$ci->db->where($where);
		
	$query = $ci->db->get();
	$result =  $query->result();
	
	
	
	foreach ($result as $row)
	{
		
		//echo $row->parent;
		
		$where = "parent = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
		$ci->db->select('SUM(jumlah) AS jumlah',FALSE);
		$ci->db->from($vwfrom);
		$ci->db->where($where);
		
		$queryParent = $ci->db->get();
		$resultParent =  $queryParent->result();
		
		foreach ($resultParent as $rowParent)
		{
			//$rowParent->jumlah;
			
			$data = array(
            'jumlah' => $rowParent->jumlah,
            'date' => time()
         );
		
		$where = $fieldWilayahRekening." = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
		$ci->db->where($where);
		$update = $ci->db->update($fromJumlah, $data);
		
		
		if($update){
		
			//$where = $fieldWilayahRekening." = '".$row->parent."' AND ".$fieldWilayah." = '".$idWilayah."'";
			
				$ci->db->select($fieldWilayahRekening.",jumlah");
				$ci->db->from($vwfrom);
				$ci->db->where($where);
			
		
			$queryParent2 = $ci->db->get();
			$resultParent2 =  $queryParent2->result();
			
			foreach ($resultParent2 as $rowParent2){
			
			if($vwfrom == "vw_apbd_provinsi_total"){
				$id = $rowParent2->idProvRekening;
			}else{
				$id = $rowParent2->idKabRekening;
			}
		
			updateParent($vwfrom,$idWilayah,$id);
				
			}
			
			
			
			
		}
		
		
		}
		
		
		
			
				
	}

	
}




?>