-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `hos_customer`;
CREATE TABLE `hos_customer` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `customer_group_id` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hos_customer` (`id`, `customer_group_id`, `name`, `create_time`, `create_user`, `edit_time`, `edit_user`) VALUES
(1,	'1',	'A.J Central Asia Raya Pt',	'2015-11-05 23:09:18',	'',	'0000-00-00 00:00:00',	''),
(2,	'2',	'A.J Sinarmas Admedika',	'2015-11-05 23:09:45',	'',	'0000-00-00 00:00:00',	''),
(3,	'3',	'A.J Sinarmas Pt',	'2015-11-05 23:09:58',	'',	'0000-00-00 00:00:00',	''),
(4,	'4',	'Abda Admedika Pt',	'2015-11-05 23:10:05',	'',	'0000-00-00 00:00:00',	'');

DROP TABLE IF EXISTS `hos_customer_group`;
CREATE TABLE `hos_customer_group` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hos_customer_group` (`id`, `name`, `create_time`, `create_user`, `edit_time`, `edit_user`) VALUES
(1,	'Assuransi Add Medika PT',	'2015-11-05 23:08:06',	'',	'0000-00-00 00:00:00',	''),
(2,	'Assuransi Jiwa Seraya PT',	'2015-11-05 23:08:15',	'',	'0000-00-00 00:00:00',	''),
(3,	'Assuransi Sinar Mas PT',	'2015-11-05 23:08:21',	'',	'0000-00-00 00:00:00',	''),
(4,	'Assuransi Kesehatan',	'2015-11-05 23:08:27',	'',	'0000-00-00 00:00:00',	'');

DROP TABLE IF EXISTS `hos_dept`;
CREATE TABLE `hos_dept` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(40) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hos_dept` (`id`, `name`, `create_time`, `create_user`, `edit_time`, `edit_user`) VALUES
(1,	'Unit Gawat Darurat',	'2015-11-05 04:03:25',	'',	'0000-00-00 00:00:00',	''),
(2,	'Klinik Poli Umum',	'2015-11-05 04:04:06',	'',	'0000-00-00 00:00:00',	'');

DROP TABLE IF EXISTS `hos_dr`;
CREATE TABLE `hos_dr` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `dept_id` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(40) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hos_dr` (`id`, `dept_id`, `name`, `create_time`, `create_user`, `edit_time`, `edit_user`) VALUES
(1,	'1',	'Eko W.S. Dsb',	'2015-11-05 04:07:41',	'',	'0000-00-00 00:00:00',	''),
(2,	'1',	'Emil R.Fadli Dr.Spkk',	'2015-11-05 04:07:54',	'',	'0000-00-00 00:00:00',	''),
(3,	'2',	'Erik Pandu Drg',	'2015-11-05 04:08:10',	'',	'0000-00-00 00:00:00',	''),
(4,	'2',	'Euis Nana Spkk',	'2015-11-05 04:08:18',	'',	'0000-00-00 00:00:00',	''),
(5,	'2',	'Haris Sumitro Spbm',	'2015-11-05 04:08:25',	'',	'0000-00-00 00:00:00',	'');

DROP TABLE IF EXISTS `hos_dr_sched`;
CREATE TABLE `hos_dr_sched` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `dr_id` varchar(40) NOT NULL,
  `t_start` datetime NOT NULL,
  `t_end` datetime NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(40) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hos_dr_sched` (`id`, `dr_id`, `t_start`, `t_end`, `create_time`, `create_user`, `edit_time`, `edit_user`) VALUES
(1,	'1',	'2015-11-05 10:00:00',	'2015-11-05 12:00:00',	'2015-11-05 04:10:49',	'',	'0000-00-00 00:00:00',	'');

DROP TABLE IF EXISTS `hos_medcase`;
CREATE TABLE `hos_medcase` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(40) NOT NULL,
  `dr_sched_id` varchar(40) NOT NULL,
  `dr_id` varchar(40) NOT NULL,
  `dept_id` varchar(40) NOT NULL,
  `care_type_id` varchar(40) NOT NULL,
  `reference_id` varchar(40) NOT NULL,
  `customer_id` varchar(40) NOT NULL,
  `customer_family` varchar(1) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_nik` varchar(40) NOT NULL,
  `customer_occupation` varchar(80) NOT NULL,
  `status` varchar(1) NOT NULL,
  `arrive_time` datetime NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hos_medcase_queue`;
CREATE TABLE `hos_medcase_queue` (
  `medcase_id` varchar(40) NOT NULL,
  `queue` int(10) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hos_patient`;
CREATE TABLE `hos_patient` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(40) NOT NULL,
  `birthdate` date NOT NULL,
  `phone` varchar(80) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hos_patient_meta`;
CREATE TABLE `hos_patient_meta` (
  `patient_id` varchar(40) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hos_reference`;
CREATE TABLE `hos_reference` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_user` varchar(80) NOT NULL,
  `edit_time` datetime NOT NULL,
  `edit_user` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hos_user`;
CREATE TABLE `hos_user` (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(150) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `img` varchar(255) NOT NULL,
  `point` int(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `level` int(1) NOT NULL,
  PRIMARY KEY (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `hos_user` (`clientId`, `name`, `phone`, `email`, `address`, `username`, `password`, `img`, `point`, `status`, `level`) VALUES
(87,	'bayu bees ',	'085693631798',	'bayu.bees@gmail.com',	'Jln cemputeng',	'bayubees',	'YmF5dWJlZXM=',	'0609-portal-2_full_600.jpg',	1050000,	1,	0);

DROP VIEW IF EXISTS `view_dr_sched`;
CREATE TABLE `view_dr_sched` (`id` int(40), `dr_id` varchar(40), `dept_id` varchar(40), `dr_name` varchar(255), `dept_name` varchar(100), `t_start` datetime, `t_end` datetime, `create_time` timestamp, `create_user` varchar(40), `edit_time` datetime, `edit_user` varchar(40));


DROP VIEW IF EXISTS `view_medcase_queue`;
CREATE TABLE `view_medcase_queue` (`medcase_id` varchar(40), `dr_sched_id` varchar(40), `patient_id` varchar(40), `dr_id` varchar(40), `dept_id` varchar(40), `customer_id` varchar(40), `queue` int(10), `patient_name` varchar(255), `dr_name` varchar(255), `dept_name` varchar(100), `customer_name` varchar(255), `t_start` datetime, `t_end` datetime, `status` varchar(1), `arrive_time` datetime, `create_time` timestamp, `create_user` varchar(80), `edit_time` datetime, `edit_user` varchar(80));


DROP TABLE IF EXISTS `view_dr_sched`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_dr_sched` AS select `hos_dr_sched`.`id` AS `id`,`hos_dr_sched`.`dr_id` AS `dr_id`,`hos_dr`.`dept_id` AS `dept_id`,`hos_dr`.`name` AS `dr_name`,`hos_dept`.`name` AS `dept_name`,`hos_dr_sched`.`t_start` AS `t_start`,`hos_dr_sched`.`t_end` AS `t_end`,`hos_dr_sched`.`create_time` AS `create_time`,`hos_dr_sched`.`create_user` AS `create_user`,`hos_dr_sched`.`edit_time` AS `edit_time`,`hos_dr_sched`.`edit_user` AS `edit_user` from ((`hos_dr_sched` join `hos_dr` on((`hos_dr_sched`.`dr_id` = `hos_dr`.`id`))) join `hos_dept` on((`hos_dr`.`dept_id` = `hos_dept`.`id`)));

DROP TABLE IF EXISTS `view_medcase_queue`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_medcase_queue` AS select `hos_medcase_queue`.`medcase_id` AS `medcase_id`,`hos_medcase`.`dr_sched_id` AS `dr_sched_id`,`hos_medcase`.`patient_id` AS `patient_id`,`view_dr_sched`.`dr_id` AS `dr_id`,`view_dr_sched`.`dept_id` AS `dept_id`,`hos_medcase`.`customer_id` AS `customer_id`,`hos_medcase_queue`.`queue` AS `queue`,`hos_patient`.`name` AS `patient_name`,`view_dr_sched`.`dr_name` AS `dr_name`,`view_dr_sched`.`dept_name` AS `dept_name`,`hos_customer`.`name` AS `customer_name`,`view_dr_sched`.`t_start` AS `t_start`,`view_dr_sched`.`t_end` AS `t_end`,`hos_medcase`.`status` AS `status`,`hos_medcase`.`arrive_time` AS `arrive_time`,`hos_medcase_queue`.`create_time` AS `create_time`,`hos_medcase_queue`.`create_user` AS `create_user`,`hos_medcase_queue`.`edit_time` AS `edit_time`,`hos_medcase_queue`.`edit_user` AS `edit_user` from ((((`hos_medcase_queue` join `hos_medcase` on((`hos_medcase_queue`.`medcase_id` = `hos_medcase`.`id`))) join `view_dr_sched` on((`hos_medcase`.`dr_sched_id` = `view_dr_sched`.`id`))) join `hos_patient` on((`hos_medcase`.`patient_id` = `hos_patient`.`id`))) join `hos_customer` on((`hos_medcase`.`customer_id` = `hos_customer`.`id`)));

-- 2015-11-10 13:19:23
